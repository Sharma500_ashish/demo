import * as express from 'express';
import * as mongoose from 'mongoose';
import UserRouter from "./routers/UserRouter";
import ItemRouter from "./routers/ItemRouter";
import bodyParser = require("body-parser");
import {getEnvironment} from "./environments/env";




export class Server
{
    public  app : express.Application = express();

    constructor()
    {
        this.setConfigurations();
        this.setRoutes();
        this.error404Handler();
        this.HandleErrors();
    }

    setConfigurations()
    {
        this.ConnectMongoDB();
        this.Configurebodyparser();

    }

    ConnectMongoDB()
    {
        const database_url = getEnvironment().db_url;
        mongoose.connect(database_url,
            {useNewUrlParser:true,useUnifiedTopology:true})
            .then(()=>{
                console.log('mongodb is connected');
            });

    }

    Configurebodyparser()
    {

        this.app.use(bodyParser.urlencoded({extended:true}));
    }


    setRoutes()
    {
        this.app.use('/src/uploads',express.static('src/uploads'));
        this.app.use('/api/user',UserRouter);
        this.app.use('/api/user/item' , ItemRouter)

    }

    error404Handler()
    {
        this.app.use((req,res)=>
        {
            res.status(404).json(
                {
                    message:'Not Found',
                    Status_code:404,
                });

        });
    }

    HandleErrors()
    {
        this.app.use((error,req,res,next)=>{

            const errorstatus = req.errorStatus || 500;

            res.status(errorstatus).json(
                {
                    message: error.message || 'Something Went Wrong. Please try Again!',
                    status_code: errorstatus,
                });



        });
    }

}
