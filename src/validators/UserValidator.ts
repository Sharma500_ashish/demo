import {body, query} from "express-validator";
import User from "../models/Users";

export class UserValidators
{
    static signUp()
    {
        return [body('email','email is required').isEmail().custom((email)=>
        {
            return User.findOne({email:email}).then(user =>{

                if(user){
                    throw new Error('User already exists');
                }
                else{
                    return true
                }
            })
        }) ,
            body('password','password is required').isAlphanumeric().
            isLength({min:8,max:20}).withMessage('password can be 8 to 20 charaters only'),
            body('username','username is required').isString()];
    }

    static updatePassword()
    {
        return [body('password','password is required').isAlphanumeric(),
            body('confirm_password','Confirm password is required').isAlphanumeric(),
            body('new_password','New password is required').isAlphanumeric()
                .custom((newPassword,{req})=>{
                    if(newPassword === req.body.confirm_password)
                    {
                        return true;

                    }else
                    {
                        req.errorStatus = 422;
                        throw new Error('Password and Confirm Password does not Matched');
                    }
                })]
    }

    static userLogin()
    {
        return[query('email','Email is Required').isEmail().custom((email,{req})=>
        {
            return User.findOne({email:email}).then(user=>
            {
                if(user)
                {
                    req.user = user;
                    return true;
                }
                else
                {
                    throw new Error('USER DOES NOT EXIST');
                }
            })
        }),
            query('password','password is Required').isAlphanumeric()]
    }


    static updateProfilePic()
    {
        return[body('profile_pic').custom((profilePic,{req})=>
        {
            if(req.file)
            {
                return true;
            }else
            {
                throw new Error('File not Uploaded');
            }
        })]
    }



}
