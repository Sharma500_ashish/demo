import {body, param, query} from "express-validator";
import Item from "../models/Items";

export class ItemValidator
{
    static addPost()
    {
        return[body('name','Name is Required').isString(),
        body('manufacture_date', 'Manufacture Date is Required').isNumeric(),
        body('rent_price','Rent Price is Required').isNumeric(),
        body('actual_price','Actual Price is Required').isNumeric()];
    }

    static getPostById()
    {
        return[param('id').custom((id,{req})=>
        {
            return Item.findOne({_id:id},{user_id:0,__v:0}).populate('comments').then((item)=>
            {
                if(item)
                {
                    req.post = item;
                    return true;
                }
                else
                {
                    throw new Error('No Item is there ');
                }
            });
        })]
    }

    static editPost()
    {
        return[body('name','Name is Required').isString(),
            body('manufacture_date', 'Manufacture Date is Required').isNumeric(),
            body('rent_price','Rent Price is Required').isNumeric(),
            body('actual_price','Actual Price is Required').isNumeric(),
            param('id').custom((id,{req})=>
            {
                return Item.findOne({_id:id,status:true}).then((item)=>
                {
                    if(item)
                    {
                        req.post = item;
                        return true;
                    }
                    else
                    {
                        throw new Error('No Item is there ');
                    }
                });
            })
        ]
    }

    static removePost()
    {
        return[param('id').custom((id,{req})=>
        {
            return Item.findOne({_id:id,status:true},{user_id:0,__v:0}).then((item)=>
            {
                if(item)
                {

                        req.post = item;
                        return true;


                }
                else
                {
                    throw new Error('No Item is there ');
                }
            });
        })]
    }



}
