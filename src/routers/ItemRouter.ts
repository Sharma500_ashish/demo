import {Router} from "express";
import {ItemValidator} from "../validators/ItemValidator";
import {GlobalMiddleWare} from "../middlewares/GlobalMiddleWare";
import {ItemController} from "../controllers/ItemController";

class ItemRouter
{
    public router : Router;

    constructor()
    {
        this.router = Router();
        this.getRoutes();
        this.postRoutes();
        this.patchRoutes();
        this.deleteRoutes();
    }

    getRoutes()
    {
        this.router.get('/me',GlobalMiddleWare.authenticate,ItemController.getPostByUser);
        this.router.get('/all',GlobalMiddleWare.authenticate,ItemController.getAllPosts);
        this.router.get('/:id',GlobalMiddleWare.authenticate,ItemValidator.getPostById(),GlobalMiddleWare.checkError,
           ItemController.getPostById);
    }


    postRoutes()
    {
        this.router.post('/add',GlobalMiddleWare.authenticate,ItemValidator.addPost()
            ,GlobalMiddleWare.checkError,ItemController.addItem);

    }
    patchRoutes()
    {
        this.router.patch('/edit/:id',GlobalMiddleWare.authenticate,ItemValidator.editPost(),GlobalMiddleWare.checkError,
           ItemController.editPost);

    }
    deleteRoutes()
    {
        this.router.delete('/remove/:id',GlobalMiddleWare.authenticate,ItemValidator.removePost(), GlobalMiddleWare.checkError,
            ItemController.removePost);

    }

}


export default new ItemRouter().router;
