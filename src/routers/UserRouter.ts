
import {Router} from "express";
import {UserController} from "../controllers/UserController";
import {UserValidators} from "../validators/UserValidator";
import {GlobalMiddleWare} from "../middlewares/GlobalMiddleWare";
import {Utils} from "../utils/Utils";

class UserRouter
{
    public router : Router;

    constructor()
    {
        this.router = Router();
        this.getRoutes();
        this.postRoutes();
        this.patchRoutes();
        this.deleteRoutes();
    }

    getRoutes()
    {
        this.router.get('/login',UserValidators.userLogin(),GlobalMiddleWare.checkError,UserController.userLogin);
    }


    postRoutes()
    {
        this.router.post('/signup',UserValidators.signUp(),GlobalMiddleWare.checkError,UserController.signUp);
    }
    patchRoutes()
    {
        this.router.patch('/update/profile',GlobalMiddleWare.authenticate,UserController.updateUser);
        this.router.patch('/update/profilePic',GlobalMiddleWare.authenticate,
            new Utils().multer.single('profile_pic'),UserValidators.updateProfilePic(),
            GlobalMiddleWare.checkError,UserController.updateProfilePic);
        this.router.patch('/update/password',GlobalMiddleWare.authenticate,UserValidators.updatePassword(),GlobalMiddleWare.checkError,
            UserController.updatePassword);

    }
    deleteRoutes(){}

}


export default new UserRouter().router;
