import {validationResult} from "express-validator";
import * as jwt from 'jsonwebtoken';
import {getEnvironment} from "../environments/env";
export class GlobalMiddleWare
{
    static checkError(req,res,next)
    {
        const error = validationResult(req);
        if(!error.isEmpty())
        {
            next(new Error(error.array()[0].msg));
        }
        else
        {
            next();
        }
    }

    static async authenticate(req,res,next)
    {
        const authHeader = req.headers.authorization;
        const token = authHeader ? authHeader.slice(7,authHeader.length) : null;
        try
        {

            jwt.verify(token,getEnvironment().jwt_secret_key,((err,decoded)=>
            {
                if(err)
                {
                    next(err);
                }else if(!decoded)
                {     req.errorStatus = 401;
                    next(new Error('User Not Authorised'));
                }else
                {

                    req.user = decoded;

                    next();
                }
            }))
        }catch (e){
            req.errorStatus = 401;
            next(e);

        }
    }

    //FOR FUTURE SCOPE -> ADMIN GUARD WILL HELP IN VALIDATE THE USERS SO THAT NORMAL USER CAN'T ACCESS ADMIN'S SERVICES


    // static adminGuard(req,res,next)
    // {
    //     const userRole = req.user.role;
    //     try
    //     {
    //         if(userRole === 'admin')
    //         {
    //             next();
    //
    //         }
    //
    //         else
    //         {
    //             next(new Error('You are not allowed to access'));
    //
    //         }
    //     }
    //     catch (e)
    //     {
    //         req.errStatus = 401;
    //         next(e);
    //     }
    // }

}
