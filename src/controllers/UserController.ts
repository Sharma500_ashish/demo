import User from '../models/Users';
import {Utils} from "../utils/Utils";
import {NodeMailer} from "../utils/NodeMailer";
import * as jwt from 'jsonwebtoken';
import {getEnvironment} from "../environments/env";


export class UserController {
    static async signUp(req, res, next) {


        const email = req.body.email;

        const username = req.body.username;
        const password = req.body.password;
        const verificationToken = Utils.genreateVerificationToken();
        const role = 'user';
        try {
            const hash = await Utils.encryptPassword(password);

            const data = {
                email: email,
                password: hash,
                username: username,
                verification_token: verificationToken,
                verification_token_time: Date.now() + new Utils().MAX_TOKEN_TIME,
                role: role
            };

            console.log(data);
            let user = await new User(data).save();
            res.send(user);
            await NodeMailer.sendEmail({
                to: ['as1996sharma@gmail.com '],
                subject: 'Email Verification',
                html: `<h1>HELLO THERE ! 😁</h1><br><h1>OTP: ${verificationToken} </h1>`
            })

        }
        catch (e) {
            next(e);

        }

    }



    static async userLogin(req, res, next) {

        const password = req.query.password;
        const user = req.user;
        try {
            await Utils.comparePassword(
                {
                    plainPassword: password
                    , encryptedPassword: user.password
                });
            const token = jwt.sign(
                {
                    email: user.email,
                    user_id: user._id,
                    username: user.username,
                    role: user.role
                },
                getEnvironment().jwt_secret_key,
                {expiresIn: '1d'});
            const data = {user: user, token: token, role: user.role};
            res.json(data);
        }
        catch (e) {
            next(e);
        }

    }

    static async updateProfilePic(req, res, next) {
        const userId = req.user.user_id;

        const searchPath = req.file.path;
        const filename = req.file.originalname;
        const replacePath = 'src/uploads/' + filename;

        const newPath = searchPath.replace(searchPath, replacePath);

        const fileUrl = 'http://localhost:5000/' + newPath;

        try {
            const user = await User.findOneAndUpdate({_id: userId},
                {
                    updated_at: new Date()
                    , profile_pic_url: fileUrl
                }, {new: true});
            res.send(user);

        } catch (e) {
            res.send(e);
        }

    }

    static async updateUser(req, res, next) {

        const data={username:req.body.username, city:req.body.city}
        try {
            const update_user = await User.findOneAndUpdate({_id: req.user.user_id}, data, {new: true});
            res.send(update_user);
            console.log(update_user);
        } catch (e) {
            next(e);
        }
    }

    static async deleteUser(req,res,next)
    {
        console.log('delete');
    }

    static async updatePassword(req, res, next) {
        const user_id = req.user.user_id;
        const password = req.body.password;
        const newPassword = req.body.new_password;

        try {

            const user: any = await User.findOne({_id: user_id})
            await Utils.comparePassword(
                {
                    plainPassword: password,
                    encryptedPassword: user.password
                });
            const encryptedPassword = await Utils.encryptPassword(newPassword);

            const newUser = await User.findOneAndUpdate({_id: user_id},
                {password: encryptedPassword}, {new: true});
            res.send(newUser);


        } catch (e) {
            next(e);
        }
    }

}
