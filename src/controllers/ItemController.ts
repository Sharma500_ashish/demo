import Item from '../models/Items';
export class ItemController
{
    static async addItem(req,res,next)
    {
        const userId = req.user.user_id;
        const manufacture_date = req.body.manufacture_date;
        const name = req.body.name;
        const rent_price = req.body.rent_price;
         const actual_price = req.body.actual_price;

        const postData = {
            user_id : userId,
           manufacture_date : manufacture_date,
            status : true,
            name: name,
            rent_price: rent_price,
            actual_price: actual_price,
            created_at : new Date(),
            updated_at : new Date()
        };

        try
        {
            const item =  await new Item(postData).save();
            res.send(item);

        }catch(e)
        {
            next(e);
        }



    }

    static async getPostByUser(req,res,next)
    {
        const userId =  req.user.user_id;
        const page = parseInt(req.query.page);

        const perPage = 2;
        let currentPageToken = page;
        let previousPageToken = page === 1 ? null : page - 1;
        let nextPageToken = page + 1;
        let totalPages;

        try
        {
            const postCount = await Item.countDocuments(
                {user_id:userId});
            totalPages = Math.ceil(postCount/perPage);
            if(totalPages === currentPageToken || totalPages === 0)
            {
                nextPageToken = null;
            }
            if(page > totalPages)
            {
                throw new Error('No More Posts To Show');
            }
            const post = await Item.find({user_id:userId},
                {user_id:0,__v:0}).limit(perPage).skip((perPage * page) - perPage);
            res.json(
                {
                    post : post,
                    nextPageToken : nextPageToken,
                    totalPage : totalPages,
                    currentPageToken : currentPageToken,
                    previousPageToken : previousPageToken

                });
        }
        catch(e)
        {
            next(e);
        }
    }

    static async getAllPosts(req,res,next)
    {
        const page = parseInt(req.query.page);

        const perPage = 2;
        let currentPageToken = page;
        let previousPageToken = page === 1 ? null : page - 1;
        let nextPageToken = page + 1;
        let totalPages;

        try
        {
            const postCount = await Item.estimatedDocumentCount();
            totalPages = Math.ceil(postCount/perPage);
            if(totalPages === currentPageToken || totalPages === 0)
            {
                nextPageToken = null;
            }
            if(page > totalPages)
            {
                throw new Error('No More Posts To show');
            }
            const post = await Item.find({},{user_id:0,__v:0}).limit(perPage).skip((perPage * page) - perPage);
            res.json(
                {
                    post : post,
                    nextPageToken : nextPageToken,
                    totalPage : totalPages,
                    currentPageToken : currentPageToken,
                    previousPageToken : previousPageToken,

                });
        }
        catch(e)
        {
            next(e);
        }

    }

    static async getPostById(req,res,next)
    {

        res.json(
            {
                post : req.item

            });
    }

    static async editPost(req,res,next)
    {
        const manufacture_date = req.body.manufacture_date;
        const name = req.body.name;
        const rent_price = req.body.rent_price;
        const actual_price = req.body.actual_price;
        const itemId = req.item.id;

        try
        {
            const  post = await Item.findOneAndUpdate({_id:itemId},
                {manufacture_date:manufacture_date, name:name,rent_price:rent_price,actual_price:actual_price,
                    updated_at:new Date()},{new:true});
            res.send(post);
        }catch (e)
        {
            next(e);
        }


    }

    static async removePost(req,res,next)
    {
        const item = req.item;
        try
        {
            await Item.findOneAndRemove({_id:item.id})  ;
            res.send(Item);
        }catch (e)
        {
            next(e);
        }
    }



}
