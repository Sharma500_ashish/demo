import * as mongoose from 'mongoose';
import {model} from "mongoose";


const itemSchema = new mongoose.Schema({
    user_id:{type:mongoose.Types.ObjectId,
        required:true},
   name:{type: String, required:true},
    rent_price:{type:Number,required:true},
    status:{type:Boolean,required:true,default:true},
    actual_price:{type:Number,required:false},
    manufacture_date:{type: Date, required:true , default:new Date()},
    created_at:{type: Date, required:true , default:new Date()},
    updated_at:{type: Date, required:true , default:new Date()},
});

export default model('items',itemSchema);
